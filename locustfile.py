import time, uuid
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    wait_time = between(1, 5)
    
    @task
    def list_coupon(self):
        resp = self.client.get("/api/v1/coupon/list?offset=0&lmimit=0", headers={"X-Session-Token": self.session_token})
        print(resp.json())

    # @task(3)
    # def view_items(self):
    #     for item_id in range(10):
    #         self.client.get(f"/item?id={item_id}", name="/item")
    #         time.sleep(1)

    # def process_game(self):
    #     self.client.post("/gamestart")
    #     time.sleep(1)
    #     self.client.post("/gameend")
        
    def on_start(self):
        resp = self.client.post("/api/v1/signin", data={"userid": uuid.uuid4()})
        session_token = resp.json()["data"]["token"]
        self.session_token = session_token